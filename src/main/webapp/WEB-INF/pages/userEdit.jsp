<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="currentUser" class="hr.fer.ZR.models.User" scope="session"/>
<jsp:useBean id="office" class="hr.fer.ZR.models.Office" scope="request"/>





<html>
<head>
    <title>USEREDIT</title>
    <jsp:include page="about.jsp"/>
</head>

<body style="background-color: #18BC9C; font-family: Montserrat">
<input type="hidden" name="un" id="username" value="${currentUser.username}">
<%office = currentUser.getOfficeID();
%>
<br><br><br>
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1 style="color: #2C3E50 ; text-transform: uppercase"> User Edit</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5 col-lg-offset-2">
                <form action="userEdit" method="post" accept-charset="utf-8" id="userEditForm">

                    <br>

                        <input type="hidden" name="type" value="REGULAR">

                        <input type="hidden" name="userID" value="${currentUser.userID}">

                    <h4 style="color: #2C3E50"> <label for="firstName">First name :</label></h4>
                        <input type="text" class="form-control" placeholder="${sessionScope.currentUser.firstName} " name="firstName" id="firstName">
                        <br>
                    <input type="submit" class="btn-primary btn"
                               style=" background-color: #1a242f" value="Change" name="firstNameb">
                        <br><br>



                    <h4 style="color: #2C3E50"><label for="lastName">Last name :</label></h4>
                        <input type="text"  class="form-control" name="lastName" id="lastName" placeholder="${sessionScope.currentUser.lastName}" value="${lastName}">
                    <br>
                    <input type="submit" class="btn-primary btn"
                               style=" background-color: #1a242f" value="Change" name="lastNameb">
                        <br><br>

                        <h4 style="color: #2C3E50"><label for="username">Username :</label></h4>
                        <input type="text"  class="form-control"  id="userName" placeholder="${sessionScope.currentUser.username}" name="username"value="${username}">
                    <br>
                    <input type="submit" class="btn-primary btn"
                               style=" background-color: #1a242f" value="Change" name="usernameb">
                        <br><br>

                            <h4 style="color: #2C3E50"><label for="email">E-mail :</label></h4>
                    <input type="email" class="form-control"  id="email" placeholder="${sessionScope.currentUser.email}"name="email"value="${email}">
                    <br>
                    <input type="submit" class="btn-primary btn"
                               style=" background-color: #1a242f" value="Change" name="emailb">
                        <br><br>

                    <c:choose>
                        <c:when test="${empty currentUser.officeID}">
                            <h4 style="color: #2C3E50">Office is not choosen yet</h4><BR><BR>
                            <a href="${pageContext.request.contextPath}/officeEdit">Add office</a>
                        </c:when>
                        <c:otherwise>

                            <h4 style="color: #2C3E50"><label>Office : </label> ${sessionScope.currentUser.officeID.nameOffice}</h4>
                           <BR>
                            <button class="btn-primary btn btn-lg" ><a style="color: white" href="${pageContext.request.contextPath}/officeEdit">Change office</a></button>
                        </c:otherwise>
                    </c:choose>
                    <br><br>
                    <input type="submit" value="Change password" class="btn-primary btn btn-lg" name="passb">
                </form>
            </div>
        </div>
    </div>
</section>
</body>
</html>

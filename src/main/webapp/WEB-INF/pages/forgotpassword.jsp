
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Forgot Password</title>
    <jsp:include page="about.jsp"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.css"/>
</head>
<body style="background-color: #18BC9C; font-family: Montserrat">

<br><br><br><br>
<div class="container">



    <h1>FORGOT PASSWORD</h1><br><br>
    <div class="row">
        <div class="col-lg-4">
            <form action="forgotpassword" method="post" accept-charset="utf-8" id="forgotpasswordform">
                <div class="row control-group">
                    <div class="form-group col-xs-12 floating-label-form-group controls">
                    <h4>    <label for="username">Please enter username : </label></h4>
                            <input type="text" name="username"  id="username" class="form-control" placeholder="Username" required>
                        <br><br>
                        <input type="submit" value="Continue" class="btn-primary btn btn-lg">
                    </div>
                </div>
            </form>
        </div>
    </div>

</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: karlo
  Date: 03/06/2017
  Time: 13:17
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="hr.fer.ZR.models.User" %>

<html>
<head>
    <title>Confirmation of identification</title>
    <jsp:include page="about.jsp"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.css"/>

</head>
    <body style="background-color: #18BC9C; font-family: Montserrat">
       <br><br><br><br>
        <div class="container">


            <h1 style="text-transform: uppercase">Confirmation of identification</h1>
            <br><br>
            <div class="row">
                <div class="col-lg-4">
                    <form action="wrongpassword" method="post" accept-charset="utf-8" id="wrongpasswordform">
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <h4><label >Username :  </label>${currentUser.username}</h4>
                                <br>
                                <h4><label>Question :   </label>${currentUser.question}</h4>
                                <br><br>
                                <h4><label>Answer : </label> <input type="text"  name="answer" class="form-control" id="answer" placeholder="Answer" required></h4>
                                <br>

                                <input type="submit" class="btn-primary btn btn-lg" value="Done">

                                <input type="hidden" name="username" id="username" value="${currentUser.username}">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="currentUser" class="hr.fer.ZR.models.User" scope="session"/>
<link href="<c:url value="css/bootstrap.css" />" rel="stylesheet">
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <!-- Custom CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Architects+Daughter' rel='stylesheet' type='text/css'>

    <!-- Theme CSS -->
    <link href="src/main/webapp/WEB-INF/pages/css/bootstrap.css" rel="stylesheet">

    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

</head>

<body id="page-top" class="index">

<!-- Navigation -->
<nav id="mainNav" style="color: white; background: #2C3E50; font-family: Montserrat, Helvetica Neue, Helvetica, Arial, sans-serif;
        text-transform: uppercase; font-weight: 700; border: none;"
        class="navbar navbar-default navbar-fixed-top navbar-custom">
    <div  class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" style="color: white;font-size: x-large" href="/">Robolog</a>
        </div>


        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-left">
                <c:if test="${sessionScope.currentUser.type=='ADMIN' || sessionScope.currentUser.type=='ORDINARY'}">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                       style="color: white;
                    border-radius: 12% 12% 12% 12%; ">Edit Profile</a>

                    <ul class="dropdown-menu"  style="background-color: #2C3E50;
                        border-radius: 12% 12% 12% 12%"><br>
                        <li><a style="color: #18BC9C " href="${pageContext.request.contextPath}/userEdit">User account</a><br></li>
                        <li><a style="color: #18BC9C " href="${pageContext.request.contextPath}/favoritePositionEdit">Favorite positions</a><br></li>
                        <li><a style="color: #18BC9C " href="${pageContext.request.contextPath}/officeEdit">Office</a></li><br>
                    </ul>
                </li>
                </c:if>

                <c:if test="${sessionScope.currentUser.type=='ADMIN'}">

                <li class="dropdown" >
                    <a href="#" class="dropdown-toggle" class="" data-toggle="dropdown" style=" color: white;
                    border-radius: 12% 12% 12% 12%">Management</a>
                    <ul class="dropdown-menu" style="background-color: #2C3E50; border-radius: 12% 12% 12% 12%">
                        <br>
                        <li><a style="color: #18BC9C " href="${pageContext.request.contextPath}/userData">Users</a><br></li>
                        <li> <a style="color: #18BC9C " href="${pageContext.request.contextPath}/confirmation">Confirm Applications</a><br></li>
                        <li><a style="color: #18BC9C " href="${pageContext.request.contextPath}/officeData">Offices</a><br>
                        </li>
                    </ul>
                </li>
                </c:if>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <c:choose>
                    <c:when test="${empty sessionScope.currentUser.userID}">
                        <li><a style="color: #18BC9C " href="${pageContext.request.contextPath}/login">Not signed in</a></li>
                    </c:when>
                    <c:otherwise>
                        <li><a style="color: #18BC9C " href="${pageContext.request.contextPath}/userEdit">
                                ${sessionScope.currentUser.firstName} ${sessionScope.currentUser.lastName}</a></li>
                        <li><a style="color: #18BC9C " href="${pageContext.request.contextPath}/logout">Sign out</a></li>
                    </c:otherwise>
                </c:choose>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

</body>
</html>
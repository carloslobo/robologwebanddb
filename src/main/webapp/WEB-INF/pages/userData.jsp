<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:useBean id="currentUser" class="hr.fer.ZR.models.User" scope="session"/>
<jsp:useBean id="user" class="hr.fer.ZR.models.User" scope="request"/>

<html>
    <head>
        <title>User Data</title>
        <jsp:include page="about.jsp"/>
    </head>

    <body style="background-color: #18BC9C; font-family: Montserrat ">
        <br><br>

        <div class="container">
            <br>
            <h1 style=" color: #2C3E50;text-transform: uppercase; font-family: Montserrat">List of users</h1>
            <br>
            <table style=" font-family: Montserrat;border-color: #2C3E50" class="table">
                <thead style="text-transform: uppercase;color: #2C3E50;">
                    <th> Korisničko ime</th>
                    <th> Ime</th>
                    <th> Prezime</th>
                    <th> E-mail</th>
                    <th> Tip korisnika</th>
                </thead>
                <tbody style="font-family: Montserrat;font-style: italic">
                    <%int i=0; %>
                    <c:forEach var="user" items="${users}">
                        <form action="userData" method="post" accept-charset="utf-8">
                            <tr>
                                <td>
                                    ${user.username}
                                </td>
                                <td>
                                    ${user.firstName}
                                </td>
                                <td>
                                    ${user.lastName}
                                </td>
                                <td>
                                    ${user.email}
                                </td>
                                <td>
                                    ${user.type}
                                </td>
                                <td>
                                    <input type="submit" value="Change Type" class="btn-primary"
                                           style="background-color: #2C3E50;outline-color: #1a242f;" name="<%=i%>">
                                </td>
                                <td>
                                    <input type="submit" value="Delete" class="btn-primary"
                                           style="background-color: #2C3E50;outline-color: #1a242f;" name="<%=i%>">
                                </td>
                            </tr>
                        </form>
                        <%i=i+1;%>
                    </c:forEach>
                </tbody>
            </table>
        </div>
</body>
</html>

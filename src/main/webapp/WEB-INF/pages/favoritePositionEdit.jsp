<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:useBean id="currentUser" class="hr.fer.ZR.models.User" scope="session"/>
<jsp:useBean id="fp" class="hr.fer.ZR.models.FavoritePosition" scope="request"/>

<html>
<head>

    <title>Favorite position edit</title>
    <jsp:include page="about.jsp"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.css"/>
</head>
<body style="background-color: #18BC9C; font-family: Montserrat">

<input type="hidden" name="un" id="username" value="${currentUser.username}">

<br><br><br><br>

<div class="container">
    <br>
    <h1 style="font-family: Montserrat; color: #2C3E50;text-transform: uppercase">Favorite Position Edit</h1>
    <br><br>
    <table style="font-family: Montserrat; color: #2C3E50" class="table">
        <thead style="text-transform: uppercase">
                <th> Name of favorite position</th>
                <th> Map</th>
        </thead>
        <tbody style="font-style: italic">
            <%int i=0; %>
            <c:forEach var="fp" items="${favoritePositions}">
                <form action="favoritePositionEdit" method="post" accept-charset="utf-8">
                    <tr>
                        <td>
                            ${fp.nameFavorite}
                        </td>
                        <td>
                            ${fp.position.mapName}
                        </td>
                        <td>
                            <input type="submit" value="Delete" class="btn-primary"
                                   style="background-color: #2C3E50;outline-color: #1a242f;" name="<%=i%>">
                        </td>
                    </tr>
                </form>

                <%i=i+1;%>
            </c:forEach>
        </tbody>
    </table>
    <label for="office" style="font-style: italic; font-family: Montserrat">Add office as favorite</label><br>
    <button id="office" class="btn-primary"
            style=" color: white; background-color: #2C3E50;outline-color: #1a242f;">
        <a style=" color: white;" href="/officeEdit">Add</a></button>
</div>


</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: karlo
  Date: 03/06/2017
  Time: 13:17
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>RESET PASSWORD</title>
    <jsp:include page="about.jsp"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.css"/>
</head>
<body style="background-color: #18BC9C; font-family: Montserrat">
<div CLASS="container">
    <br><br><br><br>
    <div class="row">
        <div class="col-lg-4">
            <form action="resetpassword" method="post" accept-charset="utf-8"  id="resetpassword">
                <div class="row control-group">
                    <div class="form-group col-xs-12 floating-label-form-group controls">

                        <input type="hidden" name="username" id="username" value="${currentUser.username}">
                        <h1 style="text-transform: uppercase">Reset password : </h1> <br>
                        <h4><label for="password">New password</label></h4>
                        <input type="password" name="password"
                               class="form-control" placeholder="Password" id="password" required>
                        <br><br>
                        <h4><label for="repeatedPassword">Repeat password : </label></h4>
                        <input type="password" name="repeatedPassword"
                               class="form-control" id="repeatedPassword" placeholder="Repeated password" required><br>
                        <input type="hidden" name="type" value="REGULAR">
                        <br>
                        <input type="submit" class="btn-primary btn btn-lg" value="Confirm">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <p><span class="errorMessage" id="errorSpan">
    <c:if test="${requestScope.notMatching}"><p class="text-danger">Lozinke se ne poklapaju!</p></c:if>
        </span></p>
</div>
</body>
</html>

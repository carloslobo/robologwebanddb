<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="currentUser" class="hr.fer.ZR.models.User" scope="session"/>
<html>
    <head>
        <title>CONFIRMATION</title>
        <jsp:include page="about.jsp"/>
    </head>

    <body style="background-color: #18BC9C;">

    <br><br><br>
        <div  class="container" >

            <h1  style=" color: #2C3E50;text-transform: uppercase; font-family: Montserrat"> KORISNICI</h1>
            <br>
            <table class="table"  style=" font-family: Montserrat;border-color: #2C3E50">
                <thead style="text-transform: uppercase;color: #2C3E50;">
                    <th>Ime</th>
                    <th>Prezime</th>
                    <th>Email</th>
                </thead>
                <tbody style="font-style: italic">
                    <%int i=0; %>
                    <c:forEach  items="${users}" var="user">
                        <form action="confirmation" method="post" accept-charset="utf-8" id="confirmationForm">
                            <tr>
                            <td>${user.firstName}</td>
                            <td>${user.lastName}</td>
                            <td>${user.email}</td>
                            <td><input type="submit" value="Confirm" class="btn-primary"
                                       style="background-color: #2C3E50;outline-color: #1a242f;"name="<%=i%>"></td>
                            <td><input type="submit" value="Delete"  class="btn-primary"
                                       style="background-color: #2C3E50;outline-color: #1a242f;" name="<%=i%>"></td>
                            </tr>
                                        <%i=i+1;%>
                        </form>
                    </c:forEach>
                </tbody>
            </table>

        </div>
    </body>
</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="office" class="hr.fer.ZR.models.Office" scope="request"/>
<jsp:useBean id="currentUser" class="hr.fer.ZR.models.User" scope="session"/>
<jsp:useBean id="user" class="hr.fer.ZR.models.User" scope="request"/>
<html>
<head>
    <title>Title</title>
    <jsp:include page="about.jsp"/>
</head>
<body  style="background-color: #18BC9C; font-family: Montserrat" >
<br><br><br>
<input type="hidden" name="un" id="username" value="${currentUser.username}">

    <div class="container">
        <form action="officeData" method="post" accept-charset="utf-8" id="officeData">
            <div class="content-section-a">
                <h1 style="text-transform: uppercase"> offices</h1>
                <br>
                <button type="button" class="page-scroll btn-primary btn page-scroll "   id="createof">
                    <a style="color: white; " href="#create">Create office</a>
                </button>
                <br><br>
                <table class="table" style="color: #2C3E50">
                    <thead style="text-transform: uppercase">
                        <th>Ime Korisnika ureda</th>
                        <th> Ime ureda</th>
                        <th> Ime mape</th>
                    </thead>
                    <tbody>
                    <%int i=0; %>
                    <c:forEach var="office" items="${offices}">
                        <tr>
                            <td>
                                <c:forEach var="user" items="${office.usersList}">
                                    ${user.firstName}<br>
                                </c:forEach>
                            </td>

                            <td>${office.nameOffice}</td>

                            <td>${office.position.mapName}</td>
                            <td>
                                <input type="submit" value="Delete" class="btn-primary btn"
                                       style=" background-color: #1a242f" name="<%=i%>">
                            </td>
                        </tr>

                        <%i=i+1;%>
                    </c:forEach>
                    </tbody>
                </table>

            </div>

            <br><br><br><br>

            <div class="container">
                <section id="create">
                    <div class="col-lg-12 ">
                        <h1 style="color: #2C3E50;text-align: center" >Create office</h1>
                        <br><br><br>
                        <hr><br><br>
                        <div class="row control-group" style="color: #2C3E50">
                            <div class="form-group col-xs-4 floating-label-form-group controls">
                                <label for="nameoffice">Name of the office :</label>
                                <input type="text" class="form-control" name="nameoffice" placeholder="Office Name " id="nameoffice"><br>

                                <label for="xcoordinate">X coordinate :</label>
                                <input type="text" class="form-control" name="xcoordinate" id="xcoordinate" placeholder="X coordinate" ><br>


                                <label for="ycoordinate">Y coordinate :</label>
                                <input type="text" name="ycoordinate" class="form-control" id="ycoordinate" placeholder="Y coordinate"><br>

                                <label for="rotation">Rotation :</label>
                                <input type="text" name="rotation" id="rotation" placeholder="Rotation" class="form-control"><br>

                                <label for="mapname">Map name :</label>
                                <input type="text" name="mapName" id="mapname" class="form-control" placeholder="Map name"><br><br><br>

                                <input type="submit" class="btn-primary btn btn-lg"
                                       value="Create" name="create"><br><br><br><br><br><br>
                            </div>
                        </div>
                    </div>
                </section>
            </div>


        </form>


    </div>



</body>
</html>

function checkPasswords(form) {
    var password = form["password"].value;
    var repeatedPassword = form["repeatedPassword"].value;

    if (password != repeatedPassword) {
        document.getElementById("errorSpan").innerHTML = "Lozinke se ne poklapaju!";
        return false;
    }

    return true;
}
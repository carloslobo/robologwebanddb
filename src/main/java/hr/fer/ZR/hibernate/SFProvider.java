package hr.fer.ZR.hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by karlo on 11/05/2017.
 */
public class SFProvider {

        private static final String CONFIG_FILE = "hibernate.cfg.xml";

        private static SessionFactory sessionFactory = null;

        private static SessionFactory initSessionFactory() {
            return new Configuration()
                    .configure(CONFIG_FILE)
                    .buildSessionFactory();
        }

        public static SessionFactory getSessionFactory() {
            if (sessionFactory == null) {
                sessionFactory = initSessionFactory();
            }

            return sessionFactory;
        }
}


package hr.fer.ZR.dao;

import hr.fer.ZR.dao.impl.FavoritePositionDAOImpl;
import hr.fer.ZR.dao.impl.OfficeDAOImpl;
import hr.fer.ZR.dao.impl.PositionDAOImpl;
import hr.fer.ZR.dao.impl.UserDAOImpl;


/**
 * Created by karlo on 19/05/2017.
 */
public final class DAOProvider {

    private static UserDAO userDAO = new UserDAOImpl();
    private static FavortePositionDAO favortePositionDAO = new FavoritePositionDAOImpl();
    private static OfficeDAO officeDAO = new OfficeDAOImpl();
    private static PositionDAO positionDAO = new PositionDAOImpl();

    public static UserDAO getUserDAO() {
        return userDAO;
    }

    public static FavortePositionDAO getFavortePositionDAO() {
        return favortePositionDAO;
    }

    public static OfficeDAO getOfficeDAO() {
        return officeDAO;
    }

    public static PositionDAO getPositionDAO() {
        return positionDAO;
    }

    private DAOProvider() {
    }
}


package hr.fer.ZR.dao.impl;

import hr.fer.ZR.dao.DAOException;
import hr.fer.ZR.dao.OfficeDAO;
import hr.fer.ZR.hibernate.SFProvider;
import hr.fer.ZR.models.Office;
import hr.fer.ZR.models.User;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by karlo on 19/05/2017.
 */
public class OfficeDAOImpl implements OfficeDAO{
    @Override
    public void addOffice(Office office) throws DAOException {
        DAOUtils.addElement(office);

    }

    @Override
    public void removeOffice(Office office) throws DAOException {
        DAOUtils.removeElement(office);
    }

    @Override
    public void modifyOffice(Office office) throws DAOException {
        DAOUtils.modifyElement(office);
    }

    @Override
    public List<Office> getAllOffices() throws DAOException {
           Session session = SFProvider.getSessionFactory().getCurrentSession();
            final String query = "SELECT u FROM Office AS u WHERE u.nameOffice=u.nameOffice";

            session.beginTransaction();
            List<Office> offices = session.createQuery(query, Office.class)
                    .setCacheable(true)
                    .list();
            session.getTransaction().commit();
//cini mi se da tu nesto fali trebalo bi pogledati..
            return offices;
    }
    public boolean officeExist(Office office) throws DAOException {
        Session session = SFProvider.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        Query<Office> query = session.createQuery("SELECT u FROM Office AS u WHERE u.nameOffice = :nameOffice", Office.class);
        query.setCacheable(true);
        query.setParameter("nameOffice",office.getNameOffice());

        Office newoffice=null;
        try{
            newoffice = query.getSingleResult();
        }catch (Exception e){
        }


        session.getTransaction().commit();

        return newoffice != null;
    }
}

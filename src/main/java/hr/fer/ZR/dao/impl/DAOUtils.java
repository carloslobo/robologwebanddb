package hr.fer.ZR.dao.impl;

import org.hibernate.Session;
import hr.fer.ZR.hibernate.SFProvider;
/**
 * Created by karlo on 22/05/2017.
 */
public final class DAOUtils {


    static <T> T findById(long id, Class<T> aClass) {
        Session session = SFProvider.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        T element = session.find(aClass, id);

        session.getTransaction().commit();

        return element;
    }

    static <T> void addElement(T element) {
        Session session = SFProvider.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        session.save(element);

        session.getTransaction().commit();
    }

    static <T> void modifyElement(T element) {
        Session session = SFProvider.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        session.merge(element);

        session.getTransaction().commit();
    }

    static <T> void removeElement(T element) {
        Session session = SFProvider.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        session.remove(element);

        session.getTransaction().commit();
    }


}

package hr.fer.ZR.dao.impl;

import hr.fer.ZR.dao.UserDAO;
import org.hibernate.Session;
import hr.fer.ZR.models.User;
import hr.fer.ZR.hibernate.SFProvider;
import hr.fer.ZR.dao.DAOException;
import org.hibernate.query.Query;

import javax.persistence.NoResultException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by karlo on 19/05/2017.
 */
public class UserDAOImpl implements UserDAO {

    @Override
    public List<User> getAllUsers(boolean bool) {
        Session session = SFProvider.getSessionFactory().getCurrentSession();
        final String query = "SELECT u FROM User AS u WHERE userID=userID";
        if(session.getTransaction().isActive())
            session.getTransaction().commit();

        session.beginTransaction();
        List<User> users = session.createQuery(query, User.class)
                .setCacheable(true)
                .list();
        session.getTransaction().commit();

        List<User> bConfirmed = new LinkedList<User>();
        for(User u : users){
         //   System.out.println(u.getFirstName()+ " "+ u.getType() +  " " + u.isConfirmed());
            if(u.isConfirmed() == bool){
                bConfirmed.add(u);
            }
        }
//cini mi se da tu nesto fali trebalo bi pogledati..
        return bConfirmed;
    }


    @Override
    public void modifyUser(User user) throws DAOException {
        DAOUtils.modifyElement(user);
    }

    @Override
    public void addUser(User user) throws DAOException {
        DAOUtils.addElement(user);
    }

    @Override
    public User getUser(long id) throws DAOException {
        return DAOUtils.findById(id, User.class);
    }

    @Override
    public User getUser(String username) throws DAOException {
        Session session = SFProvider.getSessionFactory().getCurrentSession();
        final String query = "SELECT b FROM User AS b WHERE b.username=:username";

        session.beginTransaction();

        User user= null;
        try {
            user = session.createQuery(query, User.class)
                    .setParameter("username", username)
                    .setCacheable(true)
                    .getSingleResult();
        } catch (NoResultException e) {
            session.getTransaction().commit();
            throw new DAOException(e);
        }
        session.getTransaction().commit();

        return user;
    }
    @Override
    public boolean usernameExists(String username) throws DAOException {
        Session session = SFProvider.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        Query<User> query = session.createQuery("SELECT u FROM User AS u WHERE username = :username", User.class);
        query.setCacheable(true);
        query.setParameter("username", username);

        User user = null;
        try {
            user = query.getSingleResult();
        } catch (Exception e) {
        }


        session.getTransaction().commit();

        return user != null;
    }
    @Override
    public boolean emailExists(String email) throws DAOException {
        Session session = SFProvider.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        Query<User> query = session.createQuery("SELECT u FROM User AS u WHERE email = :email", User.class);
        query.setCacheable(true);
        query.setParameter("email",email);

        User user = null;
        try{
            user = query.getSingleResult();
        }catch (Exception e){
        }


        session.getTransaction().commit();

        return user != null;
    }

    @Override
    public void removeUser(User user) throws DAOException{
        DAOUtils.removeElement(user);
    }

    @Override
    public void modifyPassword(User user) throws DAOException {
        DAOUtils.modifyElement(user);


    }

    @Override
    public void confirmUser() throws DAOException {
        //DAOUtils.modifyElement();
        //tu nesto ne stima al nema veze snaci cu se ubrzo samo da se vratim doma
    }
}

package hr.fer.ZR.dao.impl;

import hr.fer.ZR.dao.DAOException;
import hr.fer.ZR.dao.FavortePositionDAO;
import hr.fer.ZR.hibernate.SFProvider;
import hr.fer.ZR.models.FavoritePosition;
import hr.fer.ZR.models.Office;
import hr.fer.ZR.models.Position;
import hr.fer.ZR.models.User;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by karlo on 19/05/2017.
 */
public final class FavoritePositionDAOImpl implements FavortePositionDAO{

    @Override
    public List<FavoritePosition> getAllFavoritePositions(User currentuser) throws DAOException {
            Session session = SFProvider.getSessionFactory().getCurrentSession();

            session.beginTransaction();
            final String query = "SELECT u FROM FavoritePosition AS u WHERE u.userId =:currentuser";

            List<FavoritePosition> favoritePositions = session.createQuery(query, FavoritePosition.class)
                    .setParameter("currentuser", currentuser)
                    .setCacheable(true)
                    .list();
            session.getTransaction().commit();
//cini mi se da tu nesto fali trebalo bi pogledati..
            return favoritePositions;
        //djeluje dobro nisam siguran
    }


    @Override
    public void addFavorite(FavoritePosition favoritePosition) throws DAOException {
        DAOUtils.addElement(favoritePosition);
    }

    @Override
    public void removeFavorite(FavoritePosition favoritePosition) throws DAOException {
        DAOUtils.removeElement(favoritePosition);
    }
}

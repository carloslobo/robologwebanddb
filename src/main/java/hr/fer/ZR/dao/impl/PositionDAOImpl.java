package hr.fer.ZR.dao.impl;

import hr.fer.ZR.dao.DAOException;
import hr.fer.ZR.dao.PositionDAO;
import hr.fer.ZR.models.Position;

/**
 * Created by karlo on 19/05/2017.
 */
public class PositionDAOImpl implements PositionDAO {
    @Override
    public void addPosition(Position position) throws DAOException {
        DAOUtils.addElement(position);
    }

    @Override
    public Position getPosition(long id) throws DAOException {
        return DAOUtils.findById(id, Position.class);
    }

    @Override
    public void removePosition(Position position) throws DAOException {
        DAOUtils.removeElement(position);

    }
}

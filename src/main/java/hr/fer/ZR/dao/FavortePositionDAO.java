package hr.fer.ZR.dao;

import hr.fer.ZR.models.FavoritePosition;
import hr.fer.ZR.models.Office;
import hr.fer.ZR.models.User;

import java.util.List;

/**
 * Created by karlo on 19/05/2017.
 */
public interface FavortePositionDAO {

    List<FavoritePosition> getAllFavoritePositions(User user) throws DAOException;
    void addFavorite(FavoritePosition favoritePosition) throws DAOException;
    void removeFavorite(FavoritePosition favoritePosition) throws DAOException;
}

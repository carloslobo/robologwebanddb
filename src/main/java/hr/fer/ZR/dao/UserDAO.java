package hr.fer.ZR.dao;

import hr.fer.ZR.models.User;
import java.util.List;

/**
 * Created by karlo on 11/05/2017.
 */
public interface UserDAO {

    List<User> getAllUsers(boolean b) throws DAOException;
    void modifyUser(User user) throws  DAOException;
    void addUser(User user) throws DAOException;
    User getUser(long id) throws DAOException;
    User getUser(String username) throws DAOException;
    boolean usernameExists(String username) throws DAOException;
    boolean emailExists(String email) throws DAOException;
    void removeUser(User user) throws DAOException;
    void modifyPassword(User user) throws DAOException;

    //void updateOffice( ) throws DAOException;
    void confirmUser() throws DAOException;
}

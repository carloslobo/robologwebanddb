package hr.fer.ZR.dao;

import hr.fer.ZR.models.Office;

import java.util.List;

/**
 * Created by karlo on 19/05/2017.
 */
public interface OfficeDAO {
    void addOffice(Office office) throws DAOException;
    void removeOffice(Office office) throws DAOException;
    void modifyOffice (Office office) throws DAOException;
    public boolean officeExist(Office office) throws DAOException;
    List<Office> getAllOffices() throws DAOException;

}

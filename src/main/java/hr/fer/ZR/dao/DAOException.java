package hr.fer.ZR.dao;

/**
 * Created by karlo on 22/05/2017.
 */
public class DAOException extends RuntimeException{


    public DAOException(String message) {
        super(message);
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }


}

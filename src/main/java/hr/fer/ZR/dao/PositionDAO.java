package hr.fer.ZR.dao;

import hr.fer.ZR.models.Position;

/**
 * Created by karlo on 19/05/2017.
 */
public interface PositionDAO {

    void addPosition(Position position)throws DAOException;
    Position getPosition(long id) throws DAOException;
    void removePosition(Position position) throws DAOException;
}

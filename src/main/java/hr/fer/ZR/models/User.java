package hr.fer.ZR.models;

import hr.fer.ZR.dao.DAOProvider;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by karlo on 08/05/2017.
 */
@Cacheable
@Entity
@Table(name = "Users")
public class User implements Serializable{

    @Id
    @GeneratedValue
    private Long userID;

    @Column(length = 20)
    private String JMBAG=null;  //može li to ovako pitati nekoga

    @Column(nullable = false, length = 30)
    private String firstName;

    @Column(nullable = false, length = 40)
    private String lastName;

    @Column(nullable = false, length = 30, unique = true)
    private String username;

    @Column(nullable = false, length = 100, unique = true)
    private String email;

    @Column(nullable = false, length = 128)
    private String passwordHash;

    @Enumerated(EnumType.ORDINAL) //what is ORDINAL moguce->redni brojevi
    private UserType type;

    @Column(nullable = false)

    private boolean confirmed;

    @Column(nullable = false)
    private String question;

    @Column(nullable = false)
    private String answer;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date registrationDate = new Date();


    //**************
    //***RELATIONS
    //**************

    @ManyToOne
    @JoinColumn
    private Office officeID;

    @OneToMany(mappedBy = "userId", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("nameFavorite")
    private List<FavoritePosition> favorites = new ArrayList<FavoritePosition>();



    //************************
    //***GETTERS AND SETTERS
    //************************

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public String getJMBAG() {
        return JMBAG;
    }

    public void setJMBAG(String JMBAG) {
        this.JMBAG = JMBAG;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Office getOfficeID() {
        return officeID;
    }

    public void setOfficeID(Office officeID) {
        this.officeID = officeID;
    }

    public void setFavorites(List<FavoritePosition> favorites) {
        this.favorites = favorites;
    }


    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    //********************
    //***OTHER FUNCTIONS
    //********************



    public List<FavoritePosition> getFavorites() {
        return favorites;
    }

    public void addFavorite(FavoritePosition favorite) {
        DAOProvider.getFavortePositionDAO().addFavorite(favorite);
        favorites.add(favorite);
    }

    public void removeFavorite(FavoritePosition favorite) {
        favorites.remove(favorite);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return userID.equals(user.userID);
    }


    public void addFavoriteOffice(Office office) {
        FavoritePosition favoritePosition=new FavoritePosition();
        favoritePosition.setPosition(office.getPosition());
        favoritePosition.setNameFavorite(office.getNameOffice());
        favoritePosition.setOfficeID(office);
        favoritePosition.setUserId(this);
        DAOProvider.getFavortePositionDAO().addFavorite(favoritePosition);;

    }


}

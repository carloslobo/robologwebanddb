package hr.fer.ZR.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * Created by karlo on 14/05/2017.
 */
@Cacheable
@Entity
@Table(name = "Positions")
public class Position implements Serializable {

    @Id
    @GeneratedValue
    private Long posID;


    @Column(nullable = false)
    private String mapName;

    @Column(nullable = false)
    private Double xCoordinate;

    @Column(nullable = false)
    private Double yCoordinate;

    @Column()
    private Double rotation;



    //*************************
    //****GETTERS AND SETTERS
    //*************************


    public Long getPosID() {
        return posID;
    }

    public void setPosID(Long posID) {
        this.posID = posID;
    }


    public String getMapName() {
        return mapName;
    }

    public void setMapName(String mapName) {
        this.mapName = mapName;
    }

    public Double getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(Double xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public Double getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(Double yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public Double getRotation() {
        return rotation;
    }

    public void setRotation(Double rotation) {
        this.rotation = rotation;
    }
}

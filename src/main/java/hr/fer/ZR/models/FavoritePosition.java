package hr.fer.ZR.models;

/**
 * Created by karlo on 08/05/2017.
 */


import hr.fer.ZR.dao.DAOProvider;
import hr.fer.ZR.dao.impl.DAOUtils;
import javafx.geometry.Pos;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Cacheable
@Entity
@Table(name = "FavoritePosition")
public class FavoritePosition implements Serializable {

    @Id
    @GeneratedValue
    private Long fpID;

    @Column(nullable = false, length = 30)
    private String nameFavorite;


    @ManyToOne
    @JoinColumn()
    private User userId;

    @ManyToOne
    @JoinColumn
    private Office officeID;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Position position;


    //*************************
    //****GETTERS AND SETTERS
    //*************************

    public Long getFpID() {
        return fpID;
    }

    public void setFpID(Long fpID) {
        this.fpID = fpID;
    }

    public String getNameFavorite() {
        return nameFavorite;
    }

    public void setNameFavorite(String nameFavorite) {
        this.nameFavorite = nameFavorite;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Office getOfficeID() {
        return officeID;
    }

    public void setOfficeID(Office officeID) {
        this.officeID = officeID;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        Position position1 = new Position();
        position1.setxCoordinate(position.getxCoordinate());
        position1.setMapName(position.getMapName());
        position1.setyCoordinate(position.getyCoordinate());
        position1.setRotation(position.getRotation());
        DAOProvider.getPositionDAO().addPosition(position1);
        this.position = position1;
    }


    //***********************
    //******OTHER FUNCTIONS
    //***********************




}

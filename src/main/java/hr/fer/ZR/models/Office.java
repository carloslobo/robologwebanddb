package hr.fer.ZR.models;



import hr.fer.ZR.dao.DAOProvider;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * Created by karlo on 08/05/2017.
 */
@Cacheable
@Entity
@Table(name = "Offices")
public class Office implements Serializable {

    @Id
    @GeneratedValue
    private Long officeID;

    @Column(nullable = false, unique = true, length = 50)
    private String nameOffice;


    //**************
    //***RELATIONS
    //**************
    @OneToMany(mappedBy = "officeID", fetch = FetchType.LAZY)
    @OrderBy("lastName")
    private List<User> usersList = new ArrayList<User>();

    public void setOfficeID(Long officeID) {
        this.officeID = officeID;
    }

    public void setUsersList(List<User> usersList) {
        this.usersList = usersList;
    }

    public void setFavoritePositionList(List<FavoritePosition> favoritePositionList) {
        this.favoritePositionList = favoritePositionList;
    }

    public Long getOfficeID() {

        return officeID;
    }

    public List<User> getUsersList() {
        return usersList;
    }

    public List<FavoritePosition> getFavoritePositionList() {
        return favoritePositionList;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "officeID",fetch = FetchType.LAZY)
    @OrderBy("nameFavorite")
    private List<FavoritePosition> favoritePositionList = new ArrayList<FavoritePosition>();


    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn
    private Position position;


    //************************
    //***GETTERS AND SETTERS
    //************************



    public void setNameOffice(String nameOffice) {
        this.nameOffice = nameOffice;
    }

    public String getNameOffice() {
        return nameOffice;
    }

    public Position getPosition() {

        return position;
    }
    public void setPosition(Position position) {

        this.position = position;
    }

//********************
    //***OTHER FUNCTIONS
    //********************


    //**FAVORITES
    public List<FavoritePosition> getFavorites() {
        return favoritePositionList;
    }

    public void addFavorite(FavoritePosition favorite) {
        favoritePositionList.add(favorite);
    }

    public void removeFavorite(FavoritePosition favorite) {
        favoritePositionList.remove(favorite);
    }

    //**USER
    public List<User> getUser() {
        return usersList;
    }

    public void addUser(User user) {
        usersList.add(user);
    }

    public void removeuser(User user) {
        usersList.remove(user);
    }

}
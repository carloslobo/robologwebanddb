package hr.fer.ZR.web.init;

import hr.fer.ZR.hibernate.SFProvider;
import org.hibernate.SessionFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class HibernateInit implements ServletContextListener {

    //@Override
    public void contextInitialized(ServletContextEvent sce) {
        SessionFactory sessionFactory = SFProvider.getSessionFactory();
        sce.getServletContext().setAttribute("sessionFactory", sessionFactory);
    }

    //@Override
    public void contextDestroyed(ServletContextEvent sce) {
        SessionFactory sf = (SessionFactory) sce.getServletContext().getAttribute("sessionFactory");
        if (sf != null) {
            sf.close();
        }
    }
}

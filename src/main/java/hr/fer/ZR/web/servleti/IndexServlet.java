package hr.fer.ZR.web.servleti;

import hr.fer.ZR.dao.DAOProvider;
import hr.fer.ZR.models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by karlo on 08/05/2017.
 */
@WebServlet(name = "IndexServlet",urlPatterns = "/")
public class IndexServlet extends javax.servlet.http.HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.getRequestDispatcher("WEB-INF/pages/index.jsp").forward(request, response);
    }
}

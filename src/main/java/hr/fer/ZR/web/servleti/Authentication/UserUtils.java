package hr.fer.ZR.web.servleti.Authentication;

import hr.fer.ZR.hibernate.SFProvider;
import hr.fer.ZR.models.User;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.servlet.http.HttpSession;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public class UserUtils {

    /**
     * Calculates and returns the SHA-1 digest of the specified password
     * in a hex string.
     * @param password password whose digest is to be calculated
     * @return hex string representation of the passwords SHA-1 digest.
     */




    public static final int NUMBER_OF_PASSWORD_HASH_ITERATIONS = 1000;
    public static final int HASHED_PASSWORD_LENGTH = 64 * 8;
    public static final String PASSWORD_HASHING_SEED = "PBKDF2WithHmacSHA1";

    public static String generateStrongPasswordHash(char[] password, String username) throws NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] salt = getSalt(username);

        PBEKeySpec specification = new PBEKeySpec(password, salt, NUMBER_OF_PASSWORD_HASH_ITERATIONS, HASHED_PASSWORD_LENGTH);
        SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(PASSWORD_HASHING_SEED);
        byte[] hashedPasswordBytes = secretKeyFactory.generateSecret(specification).getEncoded();
        char[] hashedPassword = new char[hashedPasswordBytes.length];
        for (int i = 0; i < hashedPassword.length; i++) {
            hashedPassword[i] = (char) hashedPasswordBytes[i];

        }
        return String.valueOf(hashedPassword);
    }

    private static byte[] getSalt(String userName) throws NoSuchAlgorithmException {
        return ("robolog-" + userName).getBytes();
    }

    public static boolean passwordEquals(char[] ps1, char[] ps2 ){
        if(ps1.length!=ps2.length){
            return false;
        }else {
            for (int i = 0; i < ps2.length; i++) {
                if (ps1[i] != ps2[i]) {
                    return false;
                }

            }
        }
        return true;


    }

    /**
     * Converts the specified byte array to a hex {@code String}.
     *
     * @param input
     *            byte array to turn into a hex string.
     * @return hex string representation of the byte array.
     */
    private static String bytesToHex(byte[] input) {
        StringBuilder builder = new StringBuilder();
        for (byte b : input) {
            builder.append(String.format("%02x", b));
        }
        return builder.toString();
    }

    /**
     * Stores the user's ID, username, email, first and last name in the session,
     * effectively logging him into the application.
     *
     * @param session  session to store the user's info in
     * @param user user to 'login' to the session
     */
    static void loginUser(HttpSession session, User user) {
        SFProvider.getSessionFactory().getCurrentSession().beginTransaction();
        SFProvider.getSessionFactory().getCurrentSession().detach(user);
        SFProvider.getSessionFactory().getCurrentSession().getTransaction().commit();
        session.setAttribute("currentUser", user);
        session.setAttribute("currentUser.userID", user.getUserID());
        session.setAttribute("currentUser.username", user.getUsername());
        session.setAttribute("currentUser.firstName", user.getFirstName());
        session.setAttribute("currentUser.type",user.getType());
        session.setAttribute("currentUser.lastName", user.getLastName());
    }
    public static void refreshUser(HttpSession session, User user){
        SFProvider.getSessionFactory().getCurrentSession().beginTransaction();
        SFProvider.getSessionFactory().getCurrentSession().detach(user);
        SFProvider.getSessionFactory().getCurrentSession().getTransaction().commit();

        session.setAttribute("currentUser.userID", user.getUserID());
        session.setAttribute("currentUser", user);
        session.setAttribute("currentUser.username", user.getUsername());
        session.setAttribute("currentUser.firstName", user.getFirstName());
        session.setAttribute("currentUser.lastName", user.getLastName());
        session.setAttribute("currentUser.type",user.getType());
    }
    private UserUtils() {}
}

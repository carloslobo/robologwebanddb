package hr.fer.ZR.web.servleti;

import hr.fer.ZR.dao.DAOProvider;
import hr.fer.ZR.models.Office;
import hr.fer.ZR.models.Position;
import hr.fer.ZR.models.User;
import hr.fer.ZR.models.UserType;
import hr.fer.ZR.web.servleti.Authentication.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by karlo on 26/05/2017.
 */
@WebServlet(name = "OfficeDataServlet", urlPatterns = "/officeData")
public class OfficeDataServlet extends HttpServlet {
    List<Office> offices;
    User currentUser;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        int i=0;
        Office modoff=null;
        String value ="";
        for (Office o: offices) {
            Integer integer = i;;
            String val = (String) request.getParameter(integer.toString());
            System.out.println(val+ " "+ i + "  " + " ovdje smoooo jeaah");
            if(val!=null){
                value=val;
                modoff=o;
                break;
            }
            i++;
        }

        if(value.equals("Delete")){
            System.out.println("Brisem ove ovdi modoffice " + modoff.getNameOffice());
           if(modoff.getUsersList().size()!=0)
                for (User user : modoff.getUsersList()){
                    System.out.println(user.getFirstName());
                    user.setOfficeID(null);
                    System.out.println(user.getOfficeID()==null);
                    DAOProvider.getUserDAO().modifyUser(user);
                    System.out.println("prosao sam?");
                }
            DAOProvider.getOfficeDAO().removeOffice(modoff);
        }else{
            String v =  request.getParameter("create");
            if(v!=null) {
                if (v.equals("Create")) {

                    String nameOffice = request.getParameter("nameoffice");
                    String xcoordinate = request.getParameter("xcoordinate");
                    String ycoordinate = request.getParameter("ycoordinate");
                    String rotation = request.getParameter("rotation");
                    String mapName = request.getParameter("mapName");
                    if(nameOffice.isEmpty() || xcoordinate.isEmpty() || ycoordinate.isEmpty() || rotation.isEmpty() || mapName.isEmpty()) {
                        request.setAttribute("errorMessage", "odabrali ste postojeci ured");
                        request.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(request, response);
                    }
                    System.out.println("JESAM LI JA OVDIKA");
                    Office newoffice = new Office();
                    Position newposition = new Position();
                    newoffice.setNameOffice(nameOffice);
                    newposition.setRotation(Double.parseDouble(rotation));
                    newposition.setxCoordinate(Double.parseDouble(xcoordinate));
                    newposition.setyCoordinate(Double.parseDouble(ycoordinate));
                    newposition.setMapName(mapName);
                    newoffice.setPosition(newposition);

                    if (DAOProvider.getOfficeDAO().officeExist(newoffice)) {
                        request.setAttribute("errorMessage", "odabrali ste postojeci ured");
                        request.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(request, response);
                    } else {
                        DAOProvider.getPositionDAO().addPosition(newposition);
                        DAOProvider.getOfficeDAO().addOffice(newoffice);
                    }
                }
            }
        }
        UserUtils.refreshUser(request.getSession(), currentUser);
        response.sendRedirect("/officeData");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentUser = (User) request.getSession().getAttribute("currentUser");

        if (currentUser.getType().equals(UserType.ADMIN) && currentUser.isConfirmed() && currentUser != null) {
            offices = DAOProvider.getOfficeDAO().getAllOffices();
            request.setAttribute("offices", offices);
            request.getRequestDispatcher("WEB-INF/pages/officeData.jsp").forward(request, response);
        }else{
            System.out.println("TU sam BIA");

            request.getRequestDispatcher("WEB-INF/pages/index.jsp").forward(request, response);
        }
    }
}

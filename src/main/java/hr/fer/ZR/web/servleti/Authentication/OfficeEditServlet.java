package hr.fer.ZR.web.servleti.Authentication;

import hr.fer.ZR.dao.DAOProvider;
import hr.fer.ZR.models.FavoritePosition;
import hr.fer.ZR.models.Office;
import hr.fer.ZR.models.User;
import hr.fer.ZR.web.servleti.Authentication.UserUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by karlo on 22/05/2017.
 */

//MJENJA SE OFFICE MOGU SE DODAVATI I KAO FAVORITI
@WebServlet(name = "OfficeEditServlet", urlPatterns = "/officeEdit")
public class OfficeEditServlet extends HttpServlet {
    List<Office> offices;
    User currentUser;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        UserUtils.refreshUser(request.getSession(), currentUser);
        int i=0;

        String value ="";
        Office modoffice=null;
        for (Office office: offices) {
            Integer integer = i;
            //      System.out.println(u.getFirstName() + "  nisam ja tu ni dosa");
            String val = (String) request.getParameter(integer.toString());
            //      System.out.println(val+ " "+ i + "  " + " ovdje smoooo jeaah");
            if (val != null) {

                value = val;
                modoffice = office;
                break;
            }
            i++;
        }
        if(value.equals("Add as favorite position")){

            currentUser.addFavoriteOffice(modoffice);
            DAOProvider.getUserDAO().modifyUser(currentUser);
        }else
        if(value.equals("Add as your office")){
            currentUser.setOfficeID(modoffice);
            DAOProvider.getUserDAO().modifyUser(currentUser);
        }
        UserUtils.refreshUser(request.getSession(), currentUser);
        response.sendRedirect("/officeEdit");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentUser = (User) request.getSession().getAttribute("currentUser");
        if(currentUser.isConfirmed() && currentUser!=null) {
            offices = DAOProvider.getOfficeDAO().getAllOffices();
            List<Office> os = new LinkedList<Office>(offices);

            for (Office office : os) {
                if (currentUser.getFavorites().size() > 0)
                    for (FavoritePosition fp : currentUser.getFavorites()) {
                        if (office.getOfficeID().equals(fp.getOfficeID().getOfficeID())) {
                            offices.remove(office);
                        }
                    }
                if (currentUser.getOfficeID() != null)
                    if (office.getOfficeID().equals(currentUser.getOfficeID().getOfficeID())) {
                        offices.remove(office);
                    }
            }

            request.setAttribute("offices", offices);
            request.getRequestDispatcher("WEB-INF/pages/officeEdit.jsp").forward(request, response);
        }else{
            System.out.println("TU sam BIA");
            request.getRequestDispatcher("WEB-INF/pages/index.jsp").forward(request, response);
        }
    }
}

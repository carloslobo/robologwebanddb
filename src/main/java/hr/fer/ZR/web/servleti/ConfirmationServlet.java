package hr.fer.ZR.web.servleti;

import hr.fer.ZR.dao.DAOProvider;
import hr.fer.ZR.models.User;
import hr.fer.ZR.models.UserType;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by karlo on 22/05/2017.
 */
@WebServlet(name = "ConfirmationServlet", urlPatterns = "/confirmation")
public class ConfirmationServlet extends HttpServlet {
    List<User> unconfirmedUsers;
    User currentUser;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        int i=0;
        User moduser=null;
        String value ="";
        for (User u: unconfirmedUsers) {
            Integer integer = i;;
            String val = (String) request.getParameter(integer.toString());
            System.out.println(val+ " "+ i + "  " + " ovdje smoooo jeaah");
            if(val!=null){
                System.out.println(" ovdje je user : "+ u.getFirstName());
                value=val;
                moduser=u;
                break;
            }
            i++;
        }

        if(value.equals("Confirm")){
            moduser.setConfirmed(true);
            DAOProvider.getUserDAO().modifyUser(moduser);
        }else if(value.equals("Delete")){
            System.out.println("Brisem ove ovdi modusere " + moduser.getFirstName());
            DAOProvider.getUserDAO().removeUser(moduser);
        }
        response.sendRedirect("/confirmation");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentUser = (User) request.getSession().getAttribute("currentUser");
        if(currentUser.getType().equals(UserType.ADMIN) && currentUser.isConfirmed() && currentUser!=null){


        unconfirmedUsers = new LinkedList<User>(DAOProvider.getUserDAO().getAllUsers(false));
        request.setAttribute("users", unconfirmedUsers);
        request.getRequestDispatcher("WEB-INF/pages/confirmation.jsp").forward(request,response);

        }else{
            System.out.println("TU sam BIA");
            request.getRequestDispatcher("WEB-INF/pages/index.jsp").forward(request, response);
        }
    }
}

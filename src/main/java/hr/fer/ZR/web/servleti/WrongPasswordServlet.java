package hr.fer.ZR.web.servleti;

import hr.fer.ZR.dao.DAOProvider;
import hr.fer.ZR.models.User;
import org.bouncycastle.crypto.agreement.srp.SRP6Client;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by karlo on 22/05/2017.
 */
@WebServlet(name = "WrongPasswordServlet", urlPatterns="/wrongpassword")
public class WrongPasswordServlet extends HttpServlet {
    User user;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String direction="/index";

        String usern = (String) request.getParameter("username");
        if(usern==null) request.getRequestDispatcher("WEB-INF/pages/error.jsp").forward(request,response);
        String answer = request.getParameter("answer");
        user = DAOProvider.getUserDAO().getUser(usern);

        if(user.getAnswer().equals(answer)){
            direction="/resetpassword";
        }else {
            request.setAttribute("errorMessage", "odabrali ste postojecu email adresu");
           request.setAttribute("errorMessage", "nije dobar odgovor");
            request.getRequestDispatcher("WEB-INF/pages/error.jsp").forward(request,response);
        }
        request.setAttribute("currentUser", user);
        request.getRequestDispatcher("WEB-INF/pages/resetpassword.jsp").forward(request,response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        user =(User) request.getAttribute("currentUser");
        request.getRequestDispatcher("WEB-INF/pages/wrongpassword.jsp").forward(request,response);
    }
}

package hr.fer.ZR.web.servleti;

import hr.fer.ZR.dao.DAOProvider;
import hr.fer.ZR.models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by karlo on 03/06/2017.
 */
@WebServlet(name = "ForgotServlet", urlPatterns="/forgotpassword")
public class ForgotServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
            String u = request.getParameter("username");
            if(DAOProvider.getUserDAO().getUser(u)!= null){
                request.setAttribute("currentUser", DAOProvider.getUserDAO().getUser(u));
                request.getRequestDispatcher("WEB-INF/pages/wrongpassword.jsp").forward(request,response);
            }else{
                request.setAttribute("errorMessage","unknown username");
                response.sendRedirect("/error");
            }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getAttribute("currentUser");
        request.getRequestDispatcher("WEB-INF/pages/forgotpassword.jsp").forward(request, response);
    }


}

package hr.fer.ZR.web.servleti.Authentication;

import hr.fer.ZR.dao.DAOException;
import hr.fer.ZR.dao.DAOProvider;
import hr.fer.ZR.models.User;
import hr.fer.ZR.models.UserType;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

@WebServlet(name = "RegisterServlet", urlPatterns = "/register")
public class    RegistrationServlet extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User cu =(User) request.getSession().getAttribute("currentUser");
        System.out.println(cu);
        if(cu==null){
            request.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(request, response);

        }
        else {
            request.getRequestDispatcher("/WEB-INF/pages/register.jsp").forward(request, response);
        }



    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        String username = request.getParameter("username");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String password = request.getParameter("password");
        String repeatedPassword = request.getParameter("repeatedPassword");
        String email = request.getParameter("email");
        String jmbag = request.getParameter("JMBAG");
        String question = request.getParameter("question");
        String answer = request.getParameter("answer");


        User user = new User();
        user.setUsername(username);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        try {
            user.setPasswordHash(UserUtils.generateStrongPasswordHash(password.toCharArray(),username.toString()));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        user.setEmail(email);
        user.setJMBAG(jmbag);
        user.setQuestion(question);
        user.setAnswer(answer);
        user.setType(UserType.valueOf("ORDINARY"));

        boolean usernameExists = true;
        boolean notMatching =  true;
        boolean emailExists =  DAOProvider.getUserDAO().emailExists(user.getEmail());
        notMatching = (!repeatedPassword.equals(password));
        try{
            DAOProvider.getUserDAO().getUser(username);
        }catch (DAOException e){
            usernameExists = false;

        }

        if(usernameExists || emailExists || notMatching){
            request.setAttribute("username",username);
            request.setAttribute("firstName",firstName);
            request.setAttribute("lastName",lastName);
            request.setAttribute("email",email);
            request.setAttribute("question",question);
            request.setAttribute("answer",answer);
            request.setAttribute("JMBAG", jmbag);
            request.setAttribute("usernameExists",usernameExists);
            request.setAttribute("emailExists",emailExists);
            request.setAttribute("notMatching",notMatching);
            request.getRequestDispatcher("/WEB-INF/pages/register.jsp").forward(request, response);
            return;
        }
        DAOProvider.getUserDAO().addUser(user);


        response.sendRedirect("/index");
    }
}

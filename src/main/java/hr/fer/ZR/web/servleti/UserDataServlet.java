package hr.fer.ZR.web.servleti;

import hr.fer.ZR.dao.DAOProvider;
import hr.fer.ZR.models.User;
import hr.fer.ZR.models.UserType;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "UserDataServlet", urlPatterns = "/userData" )
public class UserDataServlet extends HttpServlet {
    List<User> users;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User currentUser = (User) request.getSession().getAttribute("currentUser");

        if(currentUser.getType().equals(UserType.ADMIN) && currentUser.isConfirmed()){
            users = DAOProvider.getUserDAO().getAllUsers(true);
            users.remove(currentUser);
            request.setAttribute("users", users);
            request.getRequestDispatcher("/WEB-INF/pages/userData.jsp").forward(request, response);
        }else{
            System.out.println("TU sam BIA");
            request.getRequestDispatcher("WEB-INF/pages/index.jsp").forward(request, response);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        int i=0;
        User moduser=null;
        String value ="";
        for (User u: users) {
            Integer integer = i;
      //      System.out.println(u.getFirstName() + "  nisam ja tu ni dosa");
            String val = (String) request.getParameter(integer.toString());
      //      System.out.println(val+ " "+ i + "  " + " ovdje smoooo jeaah");
            if(val!=null){
                System.out.println(" ovdje je user : "+ u.getFirstName());
                value=val;
                moduser=u;
                break;
            }
            i++;
        }
        if(value.equals("Change Type")){
            if(moduser.getType()==UserType.ADMIN)
                moduser.setType(UserType.ORDINARY);
            else
                moduser.setType(UserType.ADMIN);
            DAOProvider.getUserDAO().modifyUser(moduser);
        }else if(value.equals("Delete")){
    //        System.out.println("Brisem ove ovdi modusere " + moduser.getFirstName());
            DAOProvider.getUserDAO().removeUser(moduser);
        }
        response.sendRedirect("/userData");
    }

}

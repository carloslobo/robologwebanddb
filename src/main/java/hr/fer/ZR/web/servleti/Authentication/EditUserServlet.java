package hr.fer.ZR.web.servleti.Authentication;

import hr.fer.ZR.dao.DAOProvider;
import hr.fer.ZR.hibernate.SFProvider;
import hr.fer.ZR.models.User;
import org.hibernate.Session;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by karlo on 08/05/2017.
 */
@WebServlet(name = "EditUserServlet", urlPatterns = "/userEdit")
public class EditUserServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String direction="/userEdit";

        User user = DAOProvider.getUserDAO().getUser(Long.parseLong(request.getParameter("userID")));

        if(request.getParameter("usernameb")!=null) {
            if (DAOProvider.getUserDAO().usernameExists(request.getParameter("username"))) {
                direction = "/error";
                request.setAttribute("errorMessage", "Odabrali ste postojeci username!");
            } else {
                user.setUsername(request.getParameter("username"));
            }
        }

        if(request.getParameter("firstNameb")!=null){
            user.setFirstName(request.getParameter("firstName"));
        }

        if(request.getParameter("lastNameb")!=null){
            user.setLastName(request.getParameter("lastName"));
        }
        System.out.println(DAOProvider.getUserDAO().emailExists(request.getParameter("email")));
        if(request.getParameter("emailb")!=null) {
            if (DAOProvider.getUserDAO().emailExists(request.getParameter("email"))) {
                direction = "/error";
                request.setAttribute("errorMessage", "odabrali ste postojecu email adresu");
            } else {
                user.setEmail(request.getParameter("email"));
            }
        }
        if(request.getParameter("passb")!=null) {
            request.setAttribute("currentUser", user);
            request.getRequestDispatcher("WEB-INF/pages/wrongpassword.jsp").forward(request,response);
        }
        DAOProvider.getUserDAO().modifyUser(user);
        UserUtils.refreshUser(request.getSession(),user);
        response.sendRedirect(direction);

    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User currentUser = (User) request.getSession().getAttribute("currentUser");
        if( currentUser.isConfirmed() && currentUser!=null) {
            request.getRequestDispatcher("WEB-INF/pages/userEdit.jsp").forward(request, response);
        }else{
            System.out.println("TU sam BIA");
            request.getRequestDispatcher("WEB-INF/pages/index.jsp").forward(request, response);
        }
    }
}

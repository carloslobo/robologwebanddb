package hr.fer.ZR.web.servleti.Authentication;

import hr.fer.ZR.dao.DAOProvider;
import hr.fer.ZR.models.User;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * Created by karlo on 22/05/2017.
 */
@WebServlet(name = "ResetPasswordServlet",urlPatterns="/resetpassword")
public class ResetPasswordServlet extends HttpServlet {



    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        String username = request.getParameter("username");
        if(username==null) request.getRequestDispatcher("WEB-INF/pages/error.jsp").forward(request,response);

        String password = request.getParameter("password");
        String repeatedpassword = request.getParameter("repeatedPassword");
        boolean notMatching;
        User user = DAOProvider.getUserDAO().getUser(username);
        if(notMatching = (!repeatedpassword.equals(password))){
            request.setAttribute("notMatching",notMatching);
            request.setAttribute("username", username);
            request.setAttribute("currentUser", user);
            request.getRequestDispatcher("WEB-INF/pages/resetpassword.jsp").forward(request,response);
        }else {
            try {
                user.setPasswordHash(UserUtils.generateStrongPasswordHash(password.toCharArray(), username.toString()));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
            }
            DAOProvider.getUserDAO().modifyUser(user);
            request.getRequestDispatcher("WEB-INF/pages/index.jsp").forward(request,response);
        }
    }
}

package hr.fer.ZR.web.servleti.Authentication;

import hr.fer.ZR.dao.DAOProvider;
import hr.fer.ZR.dao.impl.DAOUtils;
import hr.fer.ZR.models.FavoritePosition;
import hr.fer.ZR.models.Office;
import hr.fer.ZR.models.User;
import hr.fer.ZR.models.UserType;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by karlo on 22/05/2017.
 */
@WebServlet(name = "FavotitePositionEditServlet", urlPatterns = "/favoritePositionEdit")
public class FavoritePositionEditServlet extends HttpServlet {
    User currentUser;

    List<FavoritePosition> favoritePositions;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User cu = (User) request.getSession().getAttribute("currentUser");
        currentUser = DAOProvider.getUserDAO().getUser(cu.getUserID());
        request.setCharacterEncoding("UTF-8");
        int i=0;
        FavoritePosition modfp=null;
        String value ="";
        for (FavoritePosition fp: favoritePositions) {
            Integer integer = i;
            //      System.out.println(u.getFirstName() + "  nisam ja tu ni dosa");
            String val =  request.getParameter(integer.toString());
            //      System.out.println(val+ " "+ i + "  " + " ovdje smoooo jeaah");
            if(val!=null){
     //           System.out.println(" ovdje je user : "+ u.getFirstName());
                value=val;
                modfp=fp;
                break;
            }
            i++;
        }

        if(value.equals("Delete")){
            DAOProvider.getFavortePositionDAO().removeFavorite(modfp);
            DAOProvider.getUserDAO().modifyUser(currentUser);
        }

        UserUtils.refreshUser(request.getSession(),currentUser);
        response.sendRedirect("/favoritePositionEdit");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        currentUser = (User) request.getSession().getAttribute("currentUser");

        if(currentUser.isConfirmed()&& currentUser!=null) {
            favoritePositions=DAOProvider.getFavortePositionDAO().getAllFavoritePositions(currentUser);
            request.setAttribute("favoritePositions",favoritePositions);
            request.getRequestDispatcher("WEB-INF/pages/favoritePositionEdit.jsp").forward(request,response);
        }else{
            System.out.println("TU sam BIA");
            request.getRequestDispatcher("WEB-INF/pages/index.jsp").forward(request, response);
        }
    }
}

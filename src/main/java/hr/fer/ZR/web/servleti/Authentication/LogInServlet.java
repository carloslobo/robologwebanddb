package hr.fer.ZR.web.servleti.Authentication;

import hr.fer.ZR.dao.DAOProvider;
import hr.fer.ZR.models.User;
import org.hibernate.Session;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * Created by karlo on 08/05/2017.
 */
@WebServlet(name = "LogInServlet",urlPatterns = "/login")
public class LogInServlet extends javax.servlet.http.HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        User user = null;
        try{
            user = DAOProvider.getUserDAO().getUser(username);
        }catch(Exception  e){
            //user does not exist!
        }

        String ps = null;
        try {
            ps = UserUtils.generateStrongPasswordHash(password.toCharArray(),username.toString());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        if (user == null)  {
            request.setAttribute("username", username);
            request.setAttribute("errormessage", "Username is not correct!");
            request.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(request, response);
        }else if(user.isConfirmed()==false){
            request.setAttribute("username",username);
            request.setAttribute("errormessage", "Administrator hasn't confirmed your identity jet");
            request.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(request,response);
        }else if(!UserUtils.passwordEquals(ps.toCharArray(),user.getPasswordHash().toCharArray())){
            request.setAttribute("errormessage","Password is not correct");
            request.getRequestDispatcher("WEB-INF/pages/error.jsp").forward(request,response);
        }else {

            UserUtils.loginUser(request.getSession(), user);
            response.sendRedirect("/index");
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("WEB-INF/pages/login.jsp").forward(request, response);
    }
}

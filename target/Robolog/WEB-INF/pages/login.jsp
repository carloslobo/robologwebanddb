<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html >

    <head>
        <jsp:include page="about.jsp"/>

        <title>Login Page</title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

        <!-- Custom CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Architects+Daughter' rel='stylesheet' type='text/css'>

        <!-- Theme CSS -->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.css"/>

        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    </head>
    <body style="background-color: #18BC9C; font-family: Montserrat ">
    <br><br><br><br><br>
    <div class="content-section-a">
        <div class="container">
            <div class="row">

                <div class="col-lg-5 col-lg-6" style="background-color: #2C3E50; color: white; border-radius: 25px;text-align: center">
                    <br>
                    <h2 class="section-heading">Welcome! </h2>
                    <h3 class=""section-heading>Please enter your username and password!</h3><br>
                </div>
            </div>
        </div>
    </div>
    <br><br><br>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h1 style="color: #2C3E50">Log In</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <form action="login" method="post" accept-charset="utf-8" id="loginForm">
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                             <h4>   <label for="username">Username :</label></h4>
                                <input type="text" class="form-control" placeholder="Username" name="username" id="username" required> <br>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <h4><label for="password">Password :</label></h4>
                                <input type="password" class="form-control" name="password" placeholder="Password" id="password"><br>
                                <input type="hidden" name="type" value="REGULAR">
                            </div>
                        </div>
                        <br>
                        <div id="success"></div>
                        <div class="row">
                            <div class="form-group col-xs-12">
                                <input type="submit" class="btn btn-lg" value="Login"><br><br>

                                <a href="/register" class="btn-primary btn"
                                   style="background-color: #1a242f">Register</a>
                                <a href="/forgotpassword" class="btn-primary btn"
                                   style="background-color: #1a242f">Forgot your password</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </section>

    </form>


    </body>
</html>
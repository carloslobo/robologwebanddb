<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <jsp:include page="about.jsp"/>

</head>
<body style="font-family: Montserrat;background-color: #18BC9C">
<div class="container">
    <br><br><br><br><br><br>
    <div style="background-color: #2C3E50; border-radius: 25px; text-align: center">
        <br>
    <h1 class="bigErrorMessage" style="color: white">Error!</h1>
    <br>
<h3>
    <p class="bigErrorMessage" style="color: white">${requestScope.errormessage}</p>
</h3>
        <h4><a href="index" style="color: white" >Back on index page</a>
        </h4>
        <br>
    </div>

</div>
</body>
</html>
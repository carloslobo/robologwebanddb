<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="currentUser" class="hr.fer.ZR.models.User" scope="session"/>

<html>
<head>
    <title>Registracija</title>
    <jsp:include page="about.jsp"/>


</head>

<body style="background-color: #18BC9C; font-family: Montserrat">
    <div class="container">
        <form action="register" method="post" accept-charset="utf-8" onsubmit="return checkPasswords(this);" id="registerForm">
            <br><br><br><br>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h1 style="color: #2C3E50 ; text-transform: uppercase">registration</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5 col-lg-offset-2">
                    <h4 style="color: #2C3E50"> <label for="firstName">First name :</label></h4>
                    <input type="text" class="form-control" placeholder="First name" name="firstName" id="firstName" value="${firstName}" required>
                    <br>

                    <h4 style="color: #2C3E50"><label for="lastName">Last name :</label></h4>
                    <input type="text"  class="form-control" name="lastName" id="lastName" placeholder="Last name" value="${lastName}" required>
                    <br>

                    <h4 style="color: #2C3E50"><label for="username">Username :</label></h4>
                    <input type="text"  class="form-control"  id="userName" placeholder="Username" name="username" value="${username}" required>
                    <br>

                    <h4 style="color: #2C3E50"><label for="email">E-mail :</label></h4>
                    <input type="email" class="form-control"  id="email" placeholder="E-mail"name="email"value="${email}" required>
                    <br>

                    <h4 style="color: #2C3E50"><label for="question">Question :</label></h4>
                    <input type="text" class="form-control"  id="question" placeholder="Question" name="question" value="${question}" required>
                    <br>

                    <h4 style="color: #2C3E50"><label for="answer">Answer :</label></h4>
                    <input type="text" class="form-control"  id="answer" placeholder="Answer"name="answer" value="${answer}" required>
                    <br>

                    <h4 style="color: #2C3E50"><label for="JMBAG">JMBAG :</label></h4>
                    <input type="text" class="form-control"  id="JMBAG" placeholder="JMBAG"name="JMBAG" value="${JMBAG}" >
                    <br>

                    <h4 style="color: #2C3E50"><label for="password">Password :</label></h4>
                    <input type="password" class="form-control"  id="password" placeholder="Password"name="password"  >
                    <br>

                    <h4 style="color: #2C3E50"><label for="repeatedPassword">Repeated password :</label></h4>
                    <input type="password" class="form-control"  id="repeatedPassword" placeholder="Repeated password"
                           name="repeatedPassword" required>
                    <br>



                    <input type="hidden" name="type" value="REGULAR">
                    <input type="submit" class="btn-primary btn btn-lg" value="Register">

                    <p><span class="errorMessage" id="errorSpan">
                        <c:if test="${requestScope.usernameExists}">Korisničko ime je već iskorišteno!</c:if><br>
                        <c:if test="${requestScope.emailExists}">E-mail je već iskorišten!</c:if><br>
                        <c:if test="${requestScope.notMatching}">Lozinke se ne poklapaju!</c:if>
                    </span></p>
                </div>
            </div>

        </form>
    </div>

</body>
</html>

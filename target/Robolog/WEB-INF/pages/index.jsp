<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="user" class="hr.fer.ZR.models.User" scope="request"/>
<jsp:useBean id="currentUser" class="hr.fer.ZR.models.User" scope="session"/>


<html>
<head>
    <title>Index Page</title>
    <jsp:include page="about.jsp"/>

</head>
<body style="background-color: #18BC9C; ">

<ul>
    <br><br>
    <c:choose>
    <c:when test="${sessionScope.currentUser.type=='ADMIN' || sessionScope.currentUser.type=='ORDINARY'}">
        <div class="container-fluid" style="align-content: center; background-color: #18BC9C; font-family: Montserrat, Helvetica Neue, Helvetica, Arial, sans-serif;
             text-transform: uppercase; font-size: medium; border: none;">

        <c:if test="${sessionScope.currentUser.type =='ADMIN'}">
            <br><br><br><br>
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-3"style=" background-color: #2C3E50; color: white;text-align: center;border-radius: 25px;">
                    <h3><a STYLE="color: white; vertical-align: top"
                           href="${pageContext.request.contextPath}/officeData">Manage offices</a></h3>
                    <br>
                </div>
                <div class="col-lg-2"></div>
                <div class="col-lg-3"style="background-color: #2C3E50; color: white;text-align: center; border-radius: 25px">
                    <h3><a STYLE="color: white;text-align: center;
                     vertical-align: middle" href="${pageContext.request.contextPath}/userData">Manage users</a></h3><br>

                </div>
            </div>

        </c:if>
            <br><br><br>
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-3"style="background-color: #2C3E50; color: white ;text-align: center; border-radius: 25px">
                    <h3><a STYLE="color: white;text-align: center" href="${pageContext.request.contextPath}/userEdit">Manage account</a>    </h3><br>
                </div>
                <div class="col-lg-2"></div>
                <div class="col-lg-3"style="background-color: #2C3E50; color: white ;text-align: center; border-radius: 25px">
                    <h3><a STYLE="color: white;text-align: center" href="${pageContext.request.contextPath}/officeEdit">List of offices</a>    </h3><br>
                </div>
            </div>
            <br><br><br><br>
            <div class="row">
                <div class="col-lg-3"></div>

                <div class="col-lg-6"style="background-color: #2C3E50; color: white ;text-align: center; border-radius: 25px">
                    <h3><a STYLE="color: white;text-align: center;text-transform: uppercase" href="${pageContext.request.contextPath}/favoritePositionEdit">Manage favorite positions</a></h3><br>
                </div>
                <div class="col-lg-3"></div>

            </div>
        </div>


    </c:when>
    <c:otherwise>
        <br><br><br><br>
        <div class="col-lg-5"style="background-color: #2C3E50; color: white;text-align: center; border-radius: 25px">
            <br><br>
            <h1 ><a style="color: white; text-transform: uppercase" href="${pageContext.request.contextPath}/login"> Login</a> </h1>
            <br><br>
        </div>
        <div class="col-lg-1"></div>
        <div class="col-lg-5" style="background-color: #2C3E50; color: white;text-align: center; border-radius: 25px">
            <br><br>
            <h1 ><a style="color: white; text-transform: uppercase"href="${pageContext.request.contextPath}/register">Register</a></h1>
            <br><br>
        </div>
    </c:otherwise>
    </c:choose>


</ul>
</div>


</body>
</html>

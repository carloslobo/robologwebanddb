<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="currentOffice" class="hr.fer.ZR.models.Office" scope="request"/>
<jsp:useBean id="currentUser" class="hr.fer.ZR.models.User" scope="session"/>
<jsp:useBean id="user" class="hr.fer.ZR.models.User" scope="request"/>

<html>
<head>
    <title>Office Edit</title>
    <jsp:include page="about.jsp"/>
</head>
<body style="background-color: #18BC9C; font-family: Montserrat">
<input type="hidden" name="un" id="username" value="${currentUser.username}">
<br><br><br>
<div class="container">
    <h1 style="color: #2C3E50;text-transform: uppercase; font-family: Montserrat">List of Offices</h1>
    <br>
    <table style="font-family: Montserrat" class="table">
            <thead style="text-transform: uppercase;color: #2C3E50;">
                <th>Ime Korisnika ureda</th>
                <th> Ime ureda</th>
                <th> Ime mape</th>

            </thead>
            <tbody style="font-family: Montserrat; font-style: italic">
                <%int i=0; %>
                <c:forEach var="office" items="${offices}">

                    <form action="officeEdit" method="post" accept-charset="utf-8">

                        <tr>

                            <td>
                                <c:forEach var="user" items="${office.usersList}">
                                        ${user.firstName}<br>
                                </c:forEach>
                            </td>

                            <td>${office.nameOffice}</td>

                            <td>${office.position.mapName}</td>

                            <td><input type="submit" value="Add as favorite position" class="btn-primary"
                                       style="background-color: #2C3E50;outline-color: #1a242f;" name="<%=i%>"></td>

                            <td><input type="submit" value="Add as your office" class="btn-primary"
                                       style="background-color: #2C3E50;outline-color: #1a242f;" name="<%=i%>"></td>

                        </tr>
                    </form>
                    <%i=i+1;%>
                </c:forEach>
           </tbody>
        </table>

    </div>

</body>
</html>
